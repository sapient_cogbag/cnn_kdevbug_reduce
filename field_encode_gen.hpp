#pragma once
#include <cstdint>
      
/**
* Type-safe alias to help reduce errors.
*/
struct neuron_index {
    uint64_t idx;
};

/**
* Defines a method for easily generating field sizegetters/encoders/decoders with less of the boilerplate.
* 
* The template parameter is the 
*/
template <typename _Type, typename... _Fields> 
struct _impl_FieldEncodingGenerator {
        
    template <auto _field, auto _load_preprocessor = nullptr, auto _store_preprocessor = nullptr>
    struct _impl_add_t{}; // Invalid to not pass field.

    template <typename _FieldType, _FieldType _Type::* _field>
    struct _impl_add_t <_field> { // Pass a field.
    };

    template <typename _FieldType, typename _LPPFuncType, _FieldType _Type::*_field, _LPPFuncType _lpp_fun_ptr>
    struct _impl_add_t <_field, _lpp_fun_ptr> {

    };

    _impl_add_t <&neuron_index::idx, [](){}> v;
};
